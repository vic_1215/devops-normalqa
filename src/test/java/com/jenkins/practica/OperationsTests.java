package com.jenkins.practica;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.hamcrest.CoreMatchers;
import org.junit.Test;

import com.jenkins.utils.DogsOperations;

public class OperationsTests {

	/**
	 * Checks if the operation getRandomDogImage returns a .jpg
	 */
	@Test
	public void testRandom() {
		// Instantiate DogsOperations class
		DogsOperations DogsInstance = new DogsOperations();
		String result;
		// Call getRandomDogImage operation and store the result on String
		result = DogsInstance.getRandomDogImage();
		// Assert true if the result string ends with '.jpg'
		assertThat(result, CoreMatchers.containsString(".jpg"));
	}
	
	/**
	 * Checks if the operation getBreedList returns a list of dogs (f.e. size > 0)
	 */
	@Test
	public void breedsList() {
		// Instantiate DogsOperations class
		DogsOperations DogsInstance2 = new DogsOperations();
		ArrayList lista;
		// Call getBreedList operation and store the result on ArrayList
		lista = DogsInstance2.getBreedList();
		// Assert true if the result ArrayList has size of more than 0
		assertTrue(lista.size()>0);
	}
}
